package test;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;


public class Litres {

    SelenideElement inputString = $x("//input[@data-test-id='header__search-input--desktop']");
    SelenideElement findBtn = $x("//button[@data-test-id='header__search-button--desktop']");
    SelenideElement gridElement = $x("//div[@data-test-id='art__cover--desktop']");
    SelenideElement ppdPrice = $x("//meta[@itemprop='price']");
    SelenideElement abonementPrice = $x("//div[@data-test-id='book-sale-block__abonement--wrapper']");
    SelenideElement addToCartBtn = $x("//button[@data-test-id='book__addToCartButton--desktop']");

    @Test
    public void testLitres() {
        open("https://www.litres.ru/");
        inputString.sendKeys("грокаем алгоритмы");
        findBtn.click();
        gridElement.click();
        ppdPrice.shouldBe(exist);
        abonementPrice.shouldBe(hidden);
        addToCartBtn.shouldBe(exist);
    }
}
