package test;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class Rezonit {

    SelenideElement pcbMenu = $x("//header/div[2]/div/div/ul/li[1]");
    SelenideElement inputLength = $x("//input[@id='calculator-input-1']");
    SelenideElement inputWidth = $x("//input[@id='calculator-input-2']");
    SelenideElement inputNumber = $x("//input[@id='calculator-input-3']");
    SelenideElement calculateButton = $x("//button[@id='calculate']");
    SelenideElement price = $x("//span[@id='total-price']");
    SelenideElement alert = $x("//div[@class='alert alert-danger']");

    @Test
    public void testRezonitPositive() {
        open("https://www.rezonit.ru/");
        sleep(10000);
        pcbMenu.click();
        inputLength.sendKeys("10");
        inputWidth.sendKeys("20");
        inputNumber.sendKeys("20");
        calculateButton.click();
        sleep(10000);
        price.should(exist).shouldBe(visible);
    }

    @Test
    public void testRezonitNegative() {
        open("https://www.rezonit.ru/");
        sleep(10000);
        pcbMenu.click();
        inputLength.sendKeys("10");
        inputWidth.sendKeys("-999");
        alert.shouldBe(visible).shouldHave(text("Ширина болжна быть от 5 до 475 мм"));
    }
}
